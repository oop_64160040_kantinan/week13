package com.kantinan.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Frist JFrame");
        JButton button = new JButton("Click");
        // And frame.setTitle("Frist JFrame")
        button.setBounds(130,100,100,40);
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400,500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
